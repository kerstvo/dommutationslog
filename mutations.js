var logDOMChanges = function (targetNode) {
    var observerConfig = {
        attributes: true,
        childList: true,
        characterData: true,
        subtree: true,
        attributeOldValue: true,
        characterDataOldValue: true
    };

    var init = function() {
        var observer = false;
        var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
        if (MutationObserver && window.console) {
            observer = new MutationObserver(function (mutations) {
                mutations.forEach(function (mutation) {
                    console.groupCollapsed('Changes in DOM with node:', mutation.target);
                    console.log('Affected node:', mutation.target);
                    switch (mutation.type) {
                        case 'childList':
                            var i = 0;
                            if (mutation.addedNodes.length) {
                                for (i = 0; i < mutation.addedNodes.length; i++) {
                                    console.groupCollapsed('Node added: ', mutation.addedNodes[i]);
                                    if (mutation.previousSibling || mutation.nextSibling) {
                                        console.info('Previous node: ', mutation.previousSibling);
                                        console.info('Next node: ', mutation.nextSibling);
                                    }
                                    console.groupEnd();
                                }
                            }
                            if (mutation.removedNodes.length) {
                                for (i = 0; i < mutation.removedNodes.length; i++) {
                                    console.groupCollapsed('Node removed: ', mutation.removedNodes[i]);
                                    if (mutation.previousSibling || mutation.nextSibling) {
                                        console.info('Previous node: ', mutation.previousSibling);
                                        console.info('Next node: ', mutation.nextSibling);
                                    }
                                    console.groupEnd();
                                }
                            }
                            break;
                        case 'attributes':
                            console.groupCollapsed('Attribute added: ', mutation.attributeName, ' (with namespace: ', mutation.attributeNamespace, ')');
                            console.info('Attribute new value: ', mutation.target.attributes[mutation.attributeName].value);
                            if (mutation.oldValue) {
                                console.info('Attribute old value: ', mutation.oldValue);
                            }
                            console.log('All node attributes:', mutation.target.attributes);
                            console.groupEnd();
                            break;
                    }
                    window.last_mutaion = mutation;
                    console.log('Mutation object: ', mutation)
                    console.groupEnd();
                });
            });

            observer.observe(targetNode, observerConfig);
        }

        return observer;
    };

    return init();
};
new logDOMChanges(document.body);